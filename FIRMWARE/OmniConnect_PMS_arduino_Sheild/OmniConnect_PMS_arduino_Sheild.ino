#include <Arduino.h>
#include <avr/wdt.h>
#include <SoftwareSerial.h>
#include <SoftReset.h>
#include <EEPROM.h>

# define BUTTON 2
# define RUN_LED 13
# define STATUS_LED A1
# define ENG_ON_SIG A0
# define RELAY_RPI_5V 3
# define RELAY_SCREEN_5V 4
# define RELAY_12V 5
# define RPI_ON_SIG 6
# define RPI_BUTTON 7
# define RPI_SOFT_SHUTDOWN 8
# define RPI_RESET 9
# define PI_ON_ADD 10
# define SCREEN_ON_ADD 12
# define AUDIO_SYSTEM_ADD 14
int carry = 0;
int pi_on_mem = 0;
int screen_on_mem = 0;
int audio_system_mem = 0;
int button, rpi_button, rpi_on_sig, button_state;
long button_interval = 0;
int eng_on_sig;
long previous_millis = 0;
long previous_millis_button = 0;
long current_millis_button = 0;
int led_state = 0;
char serial_command[20];
int serial_command_complete = 0;
int serial_char_count = 0;
char cmd_1[6] = {'S', 'C', '_', 'O', 'N', 'Z'};
char cmd_2[6] = {'S', 'C', '_', 'O', 'F', 'F'};
char cmd_3[6] = {'A', 'S', '_', 'O', 'N', 'Z'};
char cmd_4[6] = {'A', 'S', '_', 'O', 'F', 'F'};
char cmd_5[6] = {'P', 'I', '_', 'O', 'F', 'F'};
char cmd_6[6] = {'P', 'I', '_', 'R', 'S', 'T'};
char cmd_7[6] = {'P', 'I', '_', 'O', 'N', 'Z'};

void setup() {
  Serial.begin(9600);

  initialize_pin();
  //wdt_disable();
  //delay(1000);
  // wdt_enable(WDTO_8S);

  delay(1000);
}

void loop() {
  // wdt_reset();
  long current_millis = millis();
  read_pin();

  if (current_millis - previous_millis >= 1000) {
    led_state = ~led_state;
    digitalWrite(RUN_LED, led_state);
    //Serial.print("\nBttn:" + String(button) + ";RPI_Bttn:" + String(rpi_button) + ";Bttn_State:" + String(button_state) + ";RPI_ON_SIG:" + String(rpi_on_sig) + ";ENG_ON_SIG:");
    //Serial.print(eng_on_sig);
    //Serial.print(";Serial_Command:" + String(serial_command));
    //Serial.print(";Bttn_Interval:");
    //Serial.print(button_interval);
    //Serial.print("MEM:" + String(pi_on_mem) + ":" + String(screen_on_mem) + ":" + String(audio_system_mem));
    previous_millis = millis();
  }
  // put your main code here, to run repeatedly:
  if (Serial.available()) {
    get_character();
  }
  if (serial_command_complete == 1) {
    //Serial.println(serial_command);
    process_serial_command();
    serial_command_complete = 0;
    memset(&serial_command, 0, sizeof(serial_command));
   //Serial.flush();
  }

}




void initialize_pin() {
  pinMode(BUTTON, INPUT);
  pinMode(RUN_LED, OUTPUT);
  pinMode(STATUS_LED, OUTPUT);
  //pinMode(ENG_ON_SIG, INPUT);
  pinMode(RELAY_RPI_5V, OUTPUT);
  pinMode(RELAY_SCREEN_5V, OUTPUT);
  pinMode(RELAY_12V, OUTPUT);
  pinMode(RPI_ON_SIG, INPUT);
  pinMode(RPI_BUTTON, INPUT);
  pinMode(RPI_SOFT_SHUTDOWN, OUTPUT);
  pinMode(RPI_RESET, OUTPUT);
  digitalWrite(RUN_LED, LOW);
  digitalWrite(STATUS_LED, LOW);
  digitalWrite(RELAY_SCREEN_5V, LOW);


  digitalWrite(RELAY_RPI_5V, LOW);

  digitalWrite(STATUS_LED, LOW);
  //digitalWrite(RELAY_RPI_5V, LOW);
  //digitalWrite(RELAY_SCREEN_5V, LOW);
  //digitalWrite(RELAY_12V, LOW);
  digitalWrite(RPI_SOFT_SHUTDOWN, LOW);
  digitalWrite(RPI_RESET, LOW);


}

void read_pin() {
  button = digitalRead(BUTTON);
  //Serial.println("Button="+String(button));
  screen_on_mem=EEPROM.read(SCREEN_ON_ADD);
  eng_on_sig = analogRead(ENG_ON_SIG);
  rpi_button = digitalRead(RPI_BUTTON);
  rpi_on_sig = digitalRead(RPI_ON_SIG);
  button_state = button || rpi_button;
  if (button_state == 1) {
    previous_millis_button = millis();
    while (button_state == 1) {
      current_millis_button = millis();
      button = digitalRead(BUTTON);
      rpi_button = digitalRead(RPI_BUTTON);
      button_state = button || rpi_button;
    }
    button_interval = current_millis_button - previous_millis_button;
  }
  if (button_interval > 0 && button_interval < 500) {
    if (screen_on_mem== 1) {
      SCREEN(0);
    }
    if (screen_on_mem==0) {
      SCREEN(1);
    }

  }
  else if (button_interval > 1000 && button_interval < 3000) {
    if (rpi_on_sig==1) {
      PI_RESET();
    }

  }
  else if (button_interval > 3000) {
    if (rpi_on_sig == 0) {
      PI_ON();
    }
    else if (rpi_on_sig == 1) {
      PI_OFF();
    }

  }
  button_interval = 0;
}




void get_character() {
  char inChar = Serial.read();
  if (inChar == '$') {
    serial_command_complete = 1;
    serial_char_count = 0;
    //Serial.println("Command Complete");
    // Serial.println(serial_command);
  }
  else {
    serial_command[serial_char_count] = inChar;
    //Serial.print("\nReceived Command[" + String(serial_char_count) + "]:" + String(inChar));
    serial_char_count = serial_char_count + 1;
  }
}


void process_serial_command() {
  char temp_command[10];
  if (serial_command[0] == '#') {
    //Serial.println("Serial Command Received");
    memcpy(&temp_command[0], &serial_command[1], sizeof(temp_command));

  }
  else if (serial_command[1] == '#') {
    memcpy(&temp_command[0], &serial_command[2], sizeof(temp_command));
  }
  else {
    memset(serial_command, 0, sizeof(serial_command));
  }
  if (strncmp(temp_command, cmd_1, sizeof(cmd_1)) == 0) {
    //Serial.print("\nON SCREEN");
    SCREEN(1);

  }

  else if (strncmp(temp_command, cmd_2, sizeof(cmd_2)) == 0) {
    //Serial.print("\nOFF SCREEN");
    SCREEN(0);


  }
  else if (strncmp(temp_command, cmd_3, sizeof(cmd_3)) == 0) {

    //Serial.print("\nON AUDIO SYSTEM");
  }
  else if (strncmp(temp_command, cmd_4, sizeof(cmd_4)) == 0) {

    //Serial.print("\nOFF AUDIO SYSTEM");
  }
  else if (strncmp(temp_command, cmd_5, sizeof(cmd_5)) == 0) {
    PI_OFF();

  }
  else if (strncmp(temp_command, cmd_6, sizeof(cmd_6)) == 0) {
    PI_RESET();
  }
  else if (strncmp(temp_command, cmd_7, sizeof(cmd_7)) == 0) {
    PI_ON();
  }

  else {
    memset(temp_command, 0, sizeof(temp_command));
  }

}



void SCREEN(int value) {
  //value:0-off,1-on
  if (value == 0) {
    digitalWrite(RELAY_SCREEN_5V, LOW);
    EEPROM.write(SCREEN_ON_ADD, 0);
  }
  else if (value == 1) {
    digitalWrite(RELAY_SCREEN_5V, HIGH);
    EEPROM.write(SCREEN_ON_ADD, 1);
  }
}

void AS(int value) {
  if (value == 0) {
    digitalWrite(RELAY_12V, LOW);
    EEPROM.write(AUDIO_SYSTEM_ADD, 0);
  }
  else if (value == 1) {
    digitalWrite(RELAY_12V, HIGH);
    EEPROM.write(AUDIO_SYSTEM_ADD, 1);
  }
}

void PI_OFF() {
  digitalWrite(RPI_SOFT_SHUTDOWN, HIGH);
  delay(250);
  digitalWrite(RPI_SOFT_SHUTDOWN, LOW);
  do {

  } while (digitalRead(RPI_ON_SIG) == 1);
  delay(2000);
  digitalWrite(RELAY_12V, LOW);
  digitalWrite(RELAY_SCREEN_5V, LOW);
  delay(2000);
  digitalWrite(RELAY_RPI_5V, HIGH);
  EEPROM.write(PI_ON_ADD, 0);
  EEPROM.write(SCREEN_ON_ADD, 0);
  EEPROM.write(AUDIO_SYSTEM_ADD, 0);
}

void PI_ON() {
  if (screen_on_mem == 0) {
    digitalWrite(RELAY_SCREEN_5V, HIGH);
    EEPROM.write(SCREEN_ON_ADD, 1);
    digitalWrite(STATUS_LED, LOW);
  }
  delay(2000);
  digitalWrite(RELAY_RPI_5V, LOW);
  EEPROM.write(PI_ON_ADD, 1);
  delay(1000);

}

void PI_RESET() {
  digitalWrite(RPI_RESET, HIGH);
  delay(200);
  digitalWrite(RPI_RESET, LOW);
}
