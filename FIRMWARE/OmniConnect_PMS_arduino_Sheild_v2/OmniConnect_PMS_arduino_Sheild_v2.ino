#include <Arduino.h>
#include <avr/wdt.h>
#include <SoftwareSerial.h>
#include <SoftReset.h>
#include <EEPROM.h>

# define BUTTON 2
# define RUN_LED 13
# define STATUS_LED A1
# define ENG_ON_SIG A0
# define RELAY_RPI_5V 3
# define RELAY_SCREEN_5V 4
# define RELAY_12V 5
# define RPI_ON_SIG 6
# define RPI_AUDIO_SYSTEM_BUTTON 7
# define RPI_SOFT_SHUTDOWN 8
# define RPI_RESET 9
# define PI_ON_ADD 10
# define SCREEN_ON_ADD 12
# define AUDIO_SYSTEM_ADD 14
# define CARRY_ON_ADD 16
# define CARRY_OFF_ADD 18
# define DEBUG_MODE_ADD 20
int carry_on, carry_off, debug_mode;
int pi_on_mem = 0;
int screen_on_mem = 0;
int audio_system_mem = 0;
int button, rpi_audio_system_button, rpi_on_sig, button_state;
long button_interval = 0;
int eng_on_sig;
long previous_millis = 0;
long previous_millis_button = 0;
long current_millis_button = 0;
int led_state = 0;
char serial_command[20];
int serial_command_complete = 0;
int serial_char_count = 0;
char cmd_1[6] = {'S', 'C', '_', 'O', 'N', 'Z'};
char cmd_2[6] = {'S', 'C', '_', 'O', 'F', 'F'};
char cmd_3[6] = {'A', 'S', '_', 'O', 'N', 'Z'};
char cmd_4[6] = {'A', 'S', '_', 'O', 'F', 'F'};
char cmd_5[6] = {'P', 'I', '_', 'O', 'F', 'F'};
char cmd_6[6] = {'P', 'I', '_', 'R', 'S', 'T'};
char cmd_7[6] = {'P', 'I', '_', 'O', 'N', 'Z'};
char cmd_8[6] = {'D', 'G', '_', 'O', 'N', 'Z'};
char cmd_9[6] = {'D', 'G', '_', 'O', 'F', 'F'};


void setup() {
  Serial.begin(9600);
  read_eeprom();
  if (debug_mode == 1) {
    Serial.print("\nOmniConnect PWR button Controller");
    Serial.print("\nFirmware Version:2");
    Serial.print("\nUpdated:5.10.2018");
  }
  initialize_pin();
  //wdt_disable();
  //delay(1000);
  // wdt_enable(WDTO_8S);

  delay(1000);
}

void loop() {
  // wdt_reset();
  long current_millis = millis();
  read_eeprom();
  read_pin();

  if (current_millis - previous_millis >= 1000) {
    led_state = ~led_state;
    digitalWrite(RUN_LED, led_state);
    if (debug_mode == 1) {
      Serial.print("\nBttn:" + String(button) + ";RPI_AS:" + String(rpi_audio_system_button) + ";RPI_ON_SIG:" + String(rpi_on_sig) + ";ENG_ON_SIG:");
      Serial.print(eng_on_sig);
      Serial.print(";MEM:" + String(pi_on_mem) + ":" + String(screen_on_mem) + ":" + String(audio_system_mem));
      Serial.print(";REL:"+String(digitalRead(RELAY_RPI_5V))+":"+String(digitalRead(RELAY_SCREEN_5V))+":"+String(digitalRead(RELAY_12V)));
      Serial.flush();
    }
    previous_millis = millis();
  }
  // put your main code here, to run repeatedly:
  if (Serial.available()) {
    get_character();
  }
  if (serial_command_complete == 1) {
    //Serial.println(serial_command);
    process_serial_command();
    serial_command_complete = 0;
    memset(&serial_command, 0, sizeof(serial_command));
    //Serial.flush();
  }

}




void initialize_pin() {
  if (debug_mode == 1) {
    Serial.print("\nInitializing Pins");
  }
  pinMode(BUTTON, INPUT);
  pinMode(RUN_LED, OUTPUT);
  pinMode(STATUS_LED, OUTPUT);
  pinMode(ENG_ON_SIG, INPUT);
  pinMode(RELAY_RPI_5V, OUTPUT);
  pinMode(RELAY_SCREEN_5V, OUTPUT);
  pinMode(RELAY_12V, OUTPUT);
  pinMode(RPI_ON_SIG, INPUT);
  pinMode(RPI_AUDIO_SYSTEM_BUTTON, INPUT);
  pinMode(RPI_SOFT_SHUTDOWN, OUTPUT);
  pinMode(RPI_RESET, OUTPUT);
  digitalWrite(RUN_LED, LOW);
  digitalWrite(STATUS_LED, LOW);
  if (pi_on_mem == 1) {
    digitalWrite(RELAY_RPI_5V, LOW);
  }
  else {
    digitalWrite(RELAY_RPI_5V, HIGH);
  }
  if (screen_on_mem == 1) {
    digitalWrite(RELAY_SCREEN_5V, HIGH);
  }
  else {
    digitalWrite(RELAY_SCREEN_5V, LOW);
  }
  if (audio_system_mem == 1) {
    digitalWrite(RELAY_12V, HIGH);
  }
  else {
    digitalWrite(RELAY_12V, LOW);
  }
  digitalWrite(STATUS_LED, LOW);
  digitalWrite(RPI_SOFT_SHUTDOWN, LOW);
  digitalWrite(RPI_RESET, LOW);
  if (debug_mode == 1) {
    Serial.print(":Done");
  }

}

void read_pin() {
//  if (debug_mode == 1) {
//    Serial.print("\nRead Pin");
//  }
  button = digitalRead(BUTTON);
//  if (debug_mode == 1) {
//    Serial.println(button);
//  }
  if (button == 1) {
    process_button();
  }
  eng_on_sig = analogRead(ENG_ON_SIG);
//    if (eng_on_sig > 900) {
//      if (screen_on_mem == 0 && digitalRead(RELAY_SCREEN_5V)==0) {
//        SCREEN(1);
//      }
//    }
//    else {
//      if (screen_on_mem == 1 && digitalRead(RELAY_SCREEN_5V)==1) {
//        SCREEN(0);
//      }
//    }
  rpi_audio_system_button = digitalRead(RPI_AUDIO_SYSTEM_BUTTON);
    if (rpi_audio_system_button == 1) {
      while (digitalRead(RPI_AUDIO_SYSTEM_BUTTON)) {
  
      }
      if (audio_system_mem == 1) {
        AS(0);
      }
      else {
        AS(1);
      }
    }

//  if (debug_mode == 1) {
//    Serial.print(":Done");
//  }
}




void get_character() {
  char inChar = Serial.read();
  if (inChar == '$') {
    serial_command_complete = 1;
    serial_char_count = 0;
    //Serial.println("Command Complete");
    // Serial.println(serial_command);
  }
  else {
    serial_command[serial_char_count] = inChar;
    //Serial.print("\nReceived Command[" + String(serial_char_count) + "]:" + String(inChar));
    serial_char_count = serial_char_count + 1;
  }
}


void process_serial_command() {
  char temp_command[10];
  if (serial_command[0] == '#') {
    //Serial.println("Serial Command Received");
    memcpy(&temp_command[0], &serial_command[1], sizeof(temp_command));

  }
  else if (serial_command[1] == '#') {
    memcpy(&temp_command[0], &serial_command[2], sizeof(temp_command));
  }
  else {
    memset(serial_command, 0, sizeof(serial_command));
  }
  if (strncmp(temp_command, cmd_1, sizeof(cmd_1)) == 0) {
    if (debug_mode == 1) {
      Serial.print("\nON SCREEN");
    }
    SCREEN(1);

  }

  else if (strncmp(temp_command, cmd_2, sizeof(cmd_2)) == 0) {
    if (debug_mode == 1) {
      Serial.print("\nOFF SCREEN");
    }
    SCREEN(0);


  }
  else if (strncmp(temp_command, cmd_3, sizeof(cmd_3)) == 0) {
    if (debug_mode == 1) {
      Serial.print("\nON AUDIO SYSTEM");
    }
    AS(1);
  }
  else if (strncmp(temp_command, cmd_4, sizeof(cmd_4)) == 0) {
    if (debug_mode == 1) {
      Serial.print("\nOFF AUDIO SYSTEM");
    }
    AS(0);
    //Serial.print("\nOFF AUDIO SYSTEM");
  }
  else if (strncmp(temp_command, cmd_5, sizeof(cmd_5)) == 0) {
    if (debug_mode == 1) {
      Serial.print("\nSHUTDOWN RPI");
    }
    PI_OFF();

  }
  else if (strncmp(temp_command, cmd_6, sizeof(cmd_6)) == 0) {
    if (debug_mode == 1) {
      Serial.print("\nRESET RPI");
    }
    PI_RESET();
  }
  else if (strncmp(temp_command, cmd_7, sizeof(cmd_7)) == 0) {
    if (debug_mode == 1) {
      Serial.print("\nONRPI");
    }
    PI_ON();
  }
  else if (strncmp(temp_command, cmd_8, sizeof(cmd_8)) == 0) {
    Serial.print("\nON DEBUG MODE ");
    EEPROM.write(DEBUG_MODE_ADD, 1);
  }
  else if (strncmp(temp_command, cmd_9, sizeof(cmd_9)) == 0) {
    Serial.print("\nOFF DEBUG MODE ");
    EEPROM.write(DEBUG_MODE_ADD, 0);

  }

  else {
    memset(temp_command, 0, sizeof(temp_command));
  }

}



void SCREEN(int value) {
  //value:0-off,1-on
  if (value == 0) {
    digitalWrite(RELAY_SCREEN_5V, LOW);
    EEPROM.write(SCREEN_ON_ADD, 0);
  }
  else if (value == 1) {
    digitalWrite(RELAY_SCREEN_5V, HIGH);
    EEPROM.write(SCREEN_ON_ADD, 1);
  }
}

void AS(int value) {
  if (value == 0) {
    digitalWrite(RELAY_12V, LOW);
    EEPROM.write(AUDIO_SYSTEM_ADD, 0);
  }
  else if (value == 1) {
    digitalWrite(RELAY_12V, HIGH);
    EEPROM.write(AUDIO_SYSTEM_ADD, 1);
  }
}

void PI_OFF() {
  digitalWrite(RPI_SOFT_SHUTDOWN, HIGH);
  delay(250);
  digitalWrite(RPI_SOFT_SHUTDOWN, LOW);
  do {

  } while (digitalRead(RPI_ON_SIG) == 1);
  delay(2000);
  digitalWrite(RELAY_12V, LOW);
  digitalWrite(RELAY_SCREEN_5V, LOW);
  delay(2000);
  digitalWrite(RELAY_RPI_5V, HIGH);
  EEPROM.write(PI_ON_ADD, 0);
  EEPROM.write(SCREEN_ON_ADD, 0);
  EEPROM.write(AUDIO_SYSTEM_ADD, 0);
}

void PI_ON() {
  if (screen_on_mem == 0) {
    digitalWrite(RELAY_SCREEN_5V, HIGH);
    EEPROM.write(SCREEN_ON_ADD, 1);
    digitalWrite(STATUS_LED, LOW);
  }
  delay(2000);
  digitalWrite(RELAY_RPI_5V, LOW);
  EEPROM.write(PI_ON_ADD, 1);
  delay(1000);

}

void PI_RESET() {
  digitalWrite(RPI_RESET, HIGH);
  delay(200);
  digitalWrite(RPI_RESET, LOW);
}

void read_eeprom() {
//  if (debug_mode == 1) {
//    Serial.print("\nRead EEPROM");
//  }
  pi_on_mem = EEPROM.read(PI_ON_ADD);
  screen_on_mem = EEPROM.read(SCREEN_ON_ADD);
  audio_system_mem = EEPROM.read(AUDIO_SYSTEM_ADD);
  carry_on = EEPROM.read(CARRY_ON_ADD);
  carry_off = EEPROM.read(CARRY_OFF_ADD);
  debug_mode = EEPROM.read(DEBUG_MODE_ADD);
//  if (debug_mode == 1) {
//    Serial.print(":Done");
//  }
}


void process_button() {
  rpi_on_sig = digitalRead(RPI_ON_SIG);
  previous_millis_button = millis();
  while (button == 1) {
    current_millis_button = millis();
    button = digitalRead(BUTTON);
  }
  button_interval = current_millis_button - previous_millis_button;
  if (button_interval > 0 && button_interval < 500) {
    if (screen_on_mem == 1) {
      SCREEN(0);
    }
    if (screen_on_mem == 0) {
      SCREEN(1);
    }

  }
  else if (button_interval > 1000 && button_interval < 3000) {
    if (rpi_on_sig == 1) {
      PI_RESET();
    }

  }
  else if (button_interval > 3000) {
    if (rpi_on_sig == 0) {
      PI_ON();
    }
    else if (rpi_on_sig == 1) {
      PI_OFF();
    }
    button_interval = 0;
  }
}
