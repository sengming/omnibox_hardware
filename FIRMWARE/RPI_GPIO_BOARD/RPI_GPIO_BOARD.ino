#include <Arduino.h>
#include <avr/wdt.h>
#include <SoftwareSerial.h>
#include <SoftReset.h>
#include <EEPROM.h>


#define BRAKE_SIGNAL1 3
#define BRAKE_SIGNAL2 4
#define AC_OFF_PB   5
#define AC_AUTO_PB  7
#define AC_ACS_PB 6
#define HAZARD_LIGHT  8
#define START_STOP_PB 9
#define LED_ERR_STATUS  11
#define LED_RPI_CON 12
#define RM_3V3 13
int RK_CMD_ADD = 10;
int rk_cmd_val;
long previous_millis = 0;
int led_state = LOW;
char serial_command[20];
int serial_command_complete = 0;
int serial_char_count = 0;
char cmd_1[6] = {'A', 'C', '_', 'O', 'N', 'Z'};
char cmd_2[6] = {'A', 'C', '_', 'O', 'F', 'F'};
char cmd_3[6] = {'A', 'C', '_', 'T', 'G', 'L'};
char cmd_4[6] = {'B', 'C', '_', 'W', 'K', 'P'};
char cmd_5[6] = {'E', 'N', '_', 'O', 'N', 'Z'};
char cmd_6[6] = {'E', 'N', '_', 'T', 'G', 'L'};
char cmd_7[6] = {'H', 'Z', '_', 'O', 'N', 'Z'};
char cmd_8[6] = {'H', 'Z', '_', 'O', 'F', 'F'};
char cmd_9[6] = {'R', 'K', '_', 'O', 'N', 'Z'};
char cmd_10[6] = {'R', 'K', '_', 'O', 'F', 'F'};
char cmd_11[6] = {'R', 'K', '_', 'S', 'T', 'T'};

void setup() {
  Serial.begin(9600);
  Initialize_Pin();
  wdt_disable();
  delay(1000);
  wdt_enable(WDTO_8S);


}

void loop() {
  wdt_reset();
  if (Serial.available()) {
    get_character();
  }
  if (serial_command_complete == 1) {
    Serial.println(serial_command);
    process_serial_command();
    serial_command_complete = 0;
  }


}



void Initialize_Pin() {
  pinMode(BRAKE_SIGNAL1, OUTPUT);
  pinMode(BRAKE_SIGNAL2, OUTPUT);
  pinMode(AC_OFF_PB, OUTPUT);
  pinMode(AC_AUTO_PB, OUTPUT);
  pinMode(AC_ACS_PB, OUTPUT);
  pinMode(HAZARD_LIGHT, OUTPUT);
  pinMode(START_STOP_PB, OUTPUT);
  pinMode(LED_ERR_STATUS, OUTPUT);
  pinMode(LED_RPI_CON, OUTPUT);
  pinMode(RM_3V3, OUTPUT);
  digitalWrite(BRAKE_SIGNAL1, LOW);
  digitalWrite(BRAKE_SIGNAL2, LOW);
  digitalWrite(AC_OFF_PB, LOW);
  digitalWrite(AC_AUTO_PB, LOW);
  digitalWrite(AC_ACS_PB, LOW);
  digitalWrite(HAZARD_LIGHT, LOW);
  digitalWrite(START_STOP_PB, LOW);
  digitalWrite(LED_ERR_STATUS, HIGH);
  digitalWrite(LED_RPI_CON, HIGH);
  rk_cmd_val = EEPROM.read(RK_CMD_ADD);
  if (rk_cmd_val > 1) {
    EEPROM.write(RK_CMD_ADD, 0);
  }
  else if (rk_cmd_val == 1) {
    digitalWrite(RM_3V3, HIGH);
  }
  else {
    digitalWrite(RM_3V3, LOW);
  }


}


void get_character() {
  char inChar = Serial.read();
  if (inChar == '$') {
    serial_command_complete = 1;
    serial_char_count = 0;
    Serial.println("Command Complete");
    Serial.println(serial_command);
  }
  else {
    serial_command[serial_char_count] = inChar;
    //Serial.print("\nReceived Command[" + String(serial_char_count) + "]:" + String(inChar));
    serial_char_count = serial_char_count + 1;
  }
}


void process_serial_command() {
  char temp_command[10];
  if (serial_command[0] == '#') {
    //Serial.println("Serial Command Received");
    memcpy(&temp_command[0], &serial_command[1], sizeof(temp_command));
  }
  if (strncmp(temp_command, cmd_1, sizeof(cmd_1)) == 0) {
    //Serial.println("Command AC_ON received");
    for (int i = 0; i < 10; i++) {
      digitalWrite(AC_AUTO_PB, HIGH);
      delay(400);
      digitalWrite(AC_AUTO_PB, LOW);
      delay(400);
      //Serial.print(i);
    }

    Serial.println("OK");;
  }

  else if (strncmp(temp_command, cmd_2, sizeof(cmd_2)) == 0) {
    //Serial.println("Command AC_ON received");
    digitalWrite(AC_OFF_PB, HIGH);
    delay(400);
    digitalWrite(AC_OFF_PB, LOW);
    Serial.println("OK");
  }
  else if (strncmp(temp_command, cmd_3, sizeof(cmd_3)) == 0) {
    //Serial.println("Command AC_ON received");
    digitalWrite(AC_ACS_PB, HIGH);
    delay(1000);
    digitalWrite(AC_ACS_PB, LOW);
    Serial.println("OK");
  }
  else if (strncmp(temp_command, cmd_4, sizeof(cmd_4)) == 0) {
    //Serial.println("Command AC_ON received");
    digitalWrite(BRAKE_SIGNAL1, HIGH);
    digitalWrite(BRAKE_SIGNAL2, HIGH);
    delay(1000);
    digitalWrite(BRAKE_SIGNAL1, LOW);
    digitalWrite(BRAKE_SIGNAL2, LOW);
    Serial.println("OK");
  }
  else if (strncmp(temp_command, cmd_5, sizeof(cmd_5)) == 0) {
    //Serial.println("Command AC_ON received");
    digitalWrite(BRAKE_SIGNAL1, HIGH);
    digitalWrite(BRAKE_SIGNAL2, HIGH);
    delay(500);
    digitalWrite(START_STOP_PB, HIGH);
    delay(2000);
    digitalWrite(BRAKE_SIGNAL1, LOW);
    digitalWrite(BRAKE_SIGNAL2, LOW);
    digitalWrite(START_STOP_PB, LOW);
    Serial.println("OK");
  }
  else if (strncmp(temp_command, cmd_6, sizeof(cmd_6)) == 0) {
    //Serial.println("Command AC_ON received");
    digitalWrite(START_STOP_PB, HIGH);
    delay(1000);
    digitalWrite(START_STOP_PB, LOW);
    Serial.println("OK");
  }
  else if (strncmp(temp_command, cmd_7, sizeof(cmd_7)) == 0) {
    //Serial.println("Command AC_ON received");
    digitalWrite(HAZARD_LIGHT, HIGH);
    Serial.println("OK");
  }
  else if (strncmp(temp_command, cmd_8, sizeof(cmd_8)) == 0) {
    //Serial.println("Command AC_ON received");
    digitalWrite(HAZARD_LIGHT, LOW);
    Serial.println("OK");
  }

  else if (strncmp(temp_command, cmd_9, sizeof(cmd_9)) == 0) {
    //Serial.println("Command AC_ON received");
    rk_cmd_val = EEPROM.read(RK_CMD_ADD);
    if (rk_cmd_val == 0) {
      EEPROM.write(RK_CMD_ADD, 1);
    }
    digitalWrite(RM_3V3, HIGH);
    Serial.println("OK");
  }
  else if (strncmp(temp_command, cmd_10, sizeof(cmd_10)) == 0) {
    //Serial.println("Command AC_ON received");
    rk_cmd_val = EEPROM.read(RK_CMD_ADD);
    if (rk_cmd_val == 1) {
      EEPROM.write(RK_CMD_ADD, 0);
    }
    digitalWrite(RM_3V3, LOW);
    Serial.println("OK");
  }
  else if (strncmp(temp_command, cmd_11, sizeof(cmd_11)) == 0) {
    //Serial.println("Command AC_ON received");
    rk_cmd_val = EEPROM.read(RK_CMD_ADD);
    if (rk_cmd_val == 1) {
      Serial.print("ON");
    }
    else {
      Serial.print("OFF");
    }
  }
  else {

  }
}

