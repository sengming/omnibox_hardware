
#include <Arduino.h>
#include <avr/wdt.h>
#include <SoftwareSerial.h>
#include <SoftReset.h>
#include <EEPROM.h>


# define LED_PIN 2
# define Q1 5 //FAN+
# define Q2 6 //OFF
# define Q3 7 //FAN-
# define Q4 8 //AUTO
# define Q5 9 //A/C



long previous_millis = 0;
int led_state = LOW;
char serial_command[20];
int serial_command_complete = 0;
int serial_char_count = 0;

char cmd_1[6] = {'A', 'C', '_', 'O', 'F', 'F'};
char cmd_2[6] = {'A', 'C', '_', 'O', 'N', 'Z'};
char cmd_3[6] = {'A', 'C', '_', 'A', '/', 'C'};
char cmd_4[6] = {'A', 'C', '_', 'A', 'U', 'T'};
char cmd_5[6] = {'A', 'C', '_', 'F', 'N', '+'};
char cmd_6[6] = {'A', 'C', '_', 'F', 'N', '-'};
void setup() {
  pinMode(Q1, OUTPUT);
  pinMode(Q2, OUTPUT);
  pinMode(Q3, OUTPUT);
  pinMode(Q4, OUTPUT);
  pinMode(Q5, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(Q1, LOW);
  digitalWrite(Q2, LOW);
  digitalWrite(Q3, LOW);
  digitalWrite(Q4, LOW);
  digitalWrite(Q5, LOW);
  digitalWrite(LED_PIN, LOW);
  Serial.begin(9600);
  wdt_disable();
  delay(1000);
  wdt_enable(WDTO_8S);
  // put your setup code here, to run once:

}

void loop() {
  wdt_reset();
  long current_millis = millis();
  if (current_millis - previous_millis >= 700) {
    led_state = ~led_state;
    digitalWrite(LED_PIN, led_state);
    previous_millis = millis();
  }
  if (Serial.available()) {
    get_character();
  }
  if (serial_command_complete == 1) {
    Serial.println(serial_command);
    process_serial_command();
    serial_command_complete = 0;
  }


  // put your main code here, to run repeatedly:

}





void get_character() {
  char inChar = Serial.read();
  if (inChar == '$') {
    serial_command_complete = 1;
    serial_char_count = 0;
    //Serial.println("Command Complete");
    // Serial.println(serial_command);
  }
  else {
    serial_command[serial_char_count] = inChar;
    //Serial.print("\nReceived Command[" + String(serial_char_count) + "]:" + String(inChar));
    serial_char_count = serial_char_count + 1;
  }
}


void process_serial_command() {
  char temp_command[10];
  if (serial_command[0] == '#') {
    //Serial.println("Serial Command Received");
    memcpy(&temp_command[0], &serial_command[1], sizeof(temp_command));
    if (strncmp(temp_command, cmd_1, sizeof(cmd_1)) == 0) {
      digitalWrite(Q2, HIGH);
      delay(100);
      digitalWrite(Q2, LOW);
      Serial.println("OK");

    }
    else if (strncmp(temp_command, cmd_2, sizeof(cmd_2)) == 0) {
      for (int i = 0; i < 2; i++) {
        digitalWrite(Q1, HIGH);
        delay(100);
        digitalWrite(Q1, LOW);
        delay(50);
      }
      Serial.println("OK");
    }
    else if (strncmp(temp_command, cmd_3, sizeof(cmd_3)) == 0) {
      digitalWrite(Q5, HIGH);
      delay(100);
      digitalWrite(Q5, LOW);
      Serial.println("OK");
    }
    else if (strncmp(temp_command, cmd_4, sizeof(cmd_4)) == 0) {
      for (int i = 0; i < 10; i++) {
        //Serial.println(i);
        digitalWrite(Q4, HIGH);
        delay(100);
        digitalWrite(Q4, LOW);
        delay(100);

      }
      Serial.println("OK");
    }
    else if (strncmp(temp_command, cmd_5, sizeof(cmd_5)) == 0) {
      for (int i = 0; i <5; i++) {
        //Serial.println(i);
        digitalWrite(Q1, HIGH);
        delay(100);
        digitalWrite(Q1, LOW);
        delay(100);
      }
      Serial.println("OK");
    }
    else if (strncmp(temp_command, cmd_6, sizeof(cmd_6)) == 0) {
      digitalWrite(Q3, HIGH);
      delay(100);
      digitalWrite(Q3, LOW);
      Serial.println("OK");
    }
    else {
      memset(&serial_command, 0, sizeof(serial_command));
      memset(&temp_command, 0, sizeof(temp_command));
    }
  }
  else {
    memset(&serial_command, 0, sizeof(serial_command));
    memset(&temp_command, 0, sizeof(temp_command));
  }

}

